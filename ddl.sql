use bioskop2db;

create table Users(
    user_id int primary key not null auto_increment,
    username varchar(50) not null,
    email_id varchar(255) not null,
    password varchar(255) not null,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

create table Films(
    film_id int primary key not null auto_increment,
    name varchar(100) not null,
    is_playing boolean not null default 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

create table Seats(
    seat_id int primary key not null auto_increment,
    seat_number int not null,
    studio_name varchar(50) not null,
    is_available boolean not null default 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

create table Schedules(
    schedule_id int primary key not null auto_increment,
    film_id int not null,
    seat_id int not null,
    price int not null,
    date_show date not null,
    show_start time not null,
    show_end time not null,  
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    foreign key (film_id) REFERENCES Films(film_id),
    foreign key (seat_id) REFERENCES Seats(seat_id)

);

create table Booking(
    booking_id int primary key not null auto_increment,
    user_id int,
    schedule_id int,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    foreign key (user_id) references Users(user_id),
    foreign key (schedule_id) references Schedules(schedule_id)
);
